var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var courierSchema = new Schema({
   shop:String,
   couriers: [{
      type: String
    }]
});
module.exports =  mongoose.model('courier', courierSchema, 'couriers');
// module.exports = mongoose.model('courier', courierSchema);  