var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subdistrictSchema = new Schema({
   subdistrict_code: String,
   subdistrict_name: String,
   city_code: String,
   city_name: String,
   province_code: String,
   province_name: String,
});

module.exports =  mongoose.model('Subdistrict', subdistrictSchema, 'mapping_subdistrict');