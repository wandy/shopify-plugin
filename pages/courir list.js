import {
    Button,
    Select,
    Checkbox,
    Card,
    Form,
    FormLayout,
    Layout,
    Page,
    Stack,
    SettingToggle,
    TextField,
    TextStyle,
  } from '@shopify/polaris';
import axios from 'axios';

class CourrierList extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          checkedItems: new Map(),
          couriers: [],
          hobbies:[]
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        var value = target.value;
        
        if(target.checked){
            this.state.hobbies[value] = value;   
        }else{
            this.state.hobbies.splice(value, 1);
        }
        
    }
    

    componentDidMount() {
        axios.get(`http://localhost:3000/api/list_courier`)
        .then(res => {
          this.state.couriers = res.data.list;
          const courier = res.data.list;
          console.log(res)
          this.setState({ courier });
        })
    }

    submit(){
        console.warn(this.state.hobbies)
    }
  
    render() {
      return <div>
            <Layout>
                <Layout.AnnotatedSection
                    title="Courier List"
                    description="testing"
                >   
                <div className ="form-row">
                    <div className ="form-group col-md-6">
                        {this.state.couriers.map((item, index) => (
                        <div key={index}>
                            <label>
                            {item.name}
                            </label>
                            <input type='checkbox' id={item.code} name="hobbies" value={item.code} onChange={this.handleInputChange} ></input>
                            {/* <Checkbox name={item.name} checked={this.state.checkedItems.get(item.name)} onChange={this.handleChange} /> */}
                        </div>
                        ))}
                    </div>
                </div>

                <div className ="form-row">
                    <div className ="col-md-12 text-center">
                        <button type="submit" className ="btn btn-primary" onClick={()=>this.submit()}>Submit</button>
                    </div>
                </div>
                </Layout.AnnotatedSection>
            </Layout>
      </div>;
    }
  }
  
  export default CourrierList;