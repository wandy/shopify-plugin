import {
    Button,
    Select,
    Checkbox,
    Card,
    Form,
    FormLayout,
    Layout,
    Page,
    Stack,
    SettingToggle,
    TextField,
    TextStyle,
  } from '@shopify/polaris';
  import axios from 'axios';
  const oauth = require('axios-oauth-client');

  class AnnotatedLayout extends React.Component {
    state = {
      discount: '10%',
      persons: [],
      couriers: [],
    };

    // async test(){
    //   const getAuthorizationCode = oauth.client(axios.create(), {
    //     url: 'http://shipdeo-main-api-development-proxy.clodeo.com/oauth2/connect/token',
    //     grant_type: 'password',
    //     client_id: 'shipdeo-internal',
    //     client_secret: 'Sh1pd30',
    //     username: 'dev@clodeo.com',
    //     password: 'clodeo@123',
    //     scope: 'baz',
    //   });

    //   return getAuthorizationCode;

    // }
    
    

    componentDidMount() {
      // const getAuthorizationCode = oauth.client(axios.create(), {
      //   url: 'http://shipdeo-main-api-development-proxy.clodeo.com/oauth2/connect/token',
      //   grant_type: 'password',
      //   client_id: 'shipdeo-internal',
      //   client_secret: 'Sh1pd30',
      //   username: 'dev@clodeo.com',
      //   password: 'clodeo@123',
      //   scope: 'baz',
      // });

      // const auth = await getAuthorizationCode();
      // console.log(auth);
 
      
      // const url = 'http://shipdeo-main-api-development-proxy.clodeo.com/couriers/list';
      // axios.post(url, {
      //     headers: {
      //       'Authorization': `Bearer 137c3bb40cfa48c68da510d4e878ad69ea192131`,
      //       'Content-Type': 'application/json',
      //       'Accept' : 'application/json',
      //     }
      //   })
      //   .then(res => {
      //     //const persons = res.data;
      //     console.log(res)
      //     //this.setState({ persons });
      //   })

      axios.get(`http://localhost:3000/api/list_courier`)
        .then(res => {
          this.state.couriers = res.data.list;
          const courier = res.data.list;
          //console.log(res)
          this.setState({ courier });
        })
     
        console.log(this.state)
     
    }
  
    render() {
        const { discount, selected, api_key, api_secret, enabled } = this.state;
        //const [selected, setSelected] = useState('today');

        //const handleSelectChange = useCallback((value) => setSelected(value), []);
        const options = [
            {label: 'JNE', value: 'jne'},
            {label: 'JNT', value: 'jnt'},
            {label: 'Lion', value: 'lion'},
          ];
        const contentStatus = enabled ? 'Disable' : 'Enable';
        const textStatus = enabled ? 'enabled' : 'disabled';
  
      return (
        <Page>
          <Layout>
            <Layout.AnnotatedSection
              title="Default discount"
              description="Add a product to Sample App, it will automatically be discounted."
            >
              <Card sectioned>
                <Form onSubmit={this.handleSubmit}>
                  <FormLayout>
                    {/* <TextField
                      value={discount}
                      onChange={this.handleChange('discount')}
                      label="Discount percentage"
                      type="discount"
                    /> */}
                    <TextField
                      value={api_key}
                      onChange={this.handleChange('api_key')}
                      label="Api Key"
                      type="api_key"
                    />
                     <TextField
                      value={api_secret}
                      onChange={this.handleChange('api_secret')}
                      label="Api Secret"
                      type="api_secret"
                    />
                    <Stack distribution="trailing">
                      <Button primary submit>
                        Save
                      </Button>
                    </Stack>
                  </FormLayout>
                </Form>
              </Card>
            </Layout.AnnotatedSection>
            <Layout.AnnotatedSection
                title="Courier List"
                description="testing"
            >
                <Card sectioned>
                    <Select
                        label="Kurir"
                        options={options}
                        onChange={this.handleChange('select')}
                        value={selected}
                    />
                </Card>
            </Layout.AnnotatedSection>
            <Layout.AnnotatedSection
                title="Courier List"
                description="testing"
            >   
                {
                  this.state.couriers.map((res, index) => {
                    //console.log(res);
                    return  (
                      <div>
                        <label>{res.name}</label>
                        <input type="checkbox" key={index}  id={res.code} name={res.name} value={res.code} ></input>
                        <br/>
                      </div>
                    )
                  })
                }
                {/* <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"></input> */}
                {/* <Checkbox
                label="Basic checkbox"
                /> */}
               
            </Layout.AnnotatedSection>
            <Layout.AnnotatedSection
                title="Price updates"
                description="Temporarily disable all Sample App price updates"
            >
                <SettingToggle
                action={{
                    content: contentStatus,
                    onAction: this.handleToggle,
                }}
                enabled={enabled}
                >
                This setting is{' '}
                <TextStyle variation="strong">{textStatus}</TextStyle>.
                </SettingToggle>
            </Layout.AnnotatedSection>
          </Layout>
        </Page>
      );
    }
  
    handleSubmit = () => {
      // this.setState({
      //   discount: this.state.discount,
      //   api_key: this.state.api_key,
      //   api_secret: this.state.api_secret,
      //   select: this.state.selected,
        
      // });
      // console.log('submission', this.state);
      const { api_key, api_secret } = this.state;
      const book = {
        api_key,
        api_secret,
      };
      console.log(book);
      axios
        .get('http://localhost:3000/courier')
        .then(() => console.log('Get courier'))
        .catch(err => {
          console.error(err);
      });
  

    };
  
    handleChange = (field) => {
      return (value) => this.setState({ [field]: value });
    };

    handleToggle = () => {
        this.setState(({ enabled }) => {
            return { enabled: !enabled };
        });
    };
    
  }
  
  export default AnnotatedLayout;