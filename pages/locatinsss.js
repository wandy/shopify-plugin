import {
    Button,
    Select,
    Checkbox,
    Card,
    Form,
    FormLayout,
    Layout,
    Page,
    Stack,
    SettingToggle,
    TextField,
    TextStyle,
  } from '@shopify/polaris';
  import axios from 'axios';

class Location extends React.Component {
    state = {
        discount: '10%',
        persons: [],
        couriers: [],
    };
    
  
      componentDidMount() {
        axios.get(`http://localhost:3000/master_locations`)
        .then(res => {
           console.log(res)
        })
       
      }
    
      render() {
        const deselectedOptions = [
          {value: 'rustic', label: 'Rustic'},
          {value: 'antique', label: 'Antique'},
          {value: 'vinyl', label: 'Vinyl'},
          {value: 'vintage', label: 'Vintage'},
          {value: 'refurbished', label: 'Refurbished'},
        ];
        const [selectedOptions, setSelectedOptions] = useState([]);
        const [inputValue, setInputValue] = useState('');
        const [options, setOptions] = useState(deselectedOptions);

        const { address, province, city, subdistrict, zip_code, phone_number, selected } = this.state;

        const provinces = [
            {label: 'Jawa Barat', value: 'jabar'},
            {label: 'Jawa Tengah', value: 'jateng'},
            {label: 'DKI Jakarta', value: 'jakarta'},
          ];
        
        const cities = [
            {label: 'Bandung', value: 'bandung'},
            {label: 'Tangerang', value: 'tangerang'},
            {label: 'Yogyakarta', value: 'yogyakarta'},
          ];

        const subdistricts = [
            {label: 'Buah Batu', value: 'bubat'},
            {label: 'Soreang', value: 'soreang'},
          ];

        return (
          <Page>
            <Layout>
              <Layout.AnnotatedSection
                title="Address"
                description="Insert Your Address Store"
              >
                <Card sectioned>
                  <Form onSubmit={this.handleSubmit}>
                    <FormLayout>
                    <TextField
                      value={address}
                      onChange={this.handleChange('address')}
                      label="Adress (*)"
                      type="address"
                    />
                    <Select
                        label="Subdistrict (*)"
                        options={subdistricts}
                        onChange={this.handleChange('select')}
                        value={selected}
                    />
                     <Select
                        label="City (*)"
                        options={cities}
                        onChange={this.handleChange('select')}
                        value={selected}
                    />

                    <Select
                        label="Province (*)"
                        options={provinces}
                        onChange={this.handleChange('select')}
                        value={selected}
                    />
                    <TextField
                      value={zip_code}
                      onChange={this.handleChange('api_secret')}
                      label="Zip Code (*)"
                      type="zipcode"
                    />
                    <TextField
                      value={phone_number}
                      onChange={this.handleChange('phone_number')}
                      label="Phone Number (*)"
                      type="phone_number"
                    />
                      <Stack distribution="trailing">
                        <Button primary submit>
                          Save
                        </Button>
                      </Stack>
                    </FormLayout>
                  </Form>
                </Card>
              </Layout.AnnotatedSection>
            </Layout>
          </Page>
        );
      }
    
      handleSubmit = () => {
        const { address, province, city, subdistrict, zip_code, phone_number } = this.state;
        const data_address = {
          address,
          province,
          city,
          subdistrict,
          zip_code,
          phone_number
        };
      };
    
      handleChange = (field) => {
        return (value) => this.setState({ [field]: value });
      };
  
      handleToggle = () => {
          this.setState(({ enabled }) => {
              return { enabled: !enabled };
          });
      };
      
    }
    
  export default Location;