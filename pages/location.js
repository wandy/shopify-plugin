import React, {useCallback, useState, useEffect} from 'react';
import {Navigation} from '@shopify/polaris';
import {HomeMajorMonotone, OnlineStoreMajorTwotone, OrdersMajorTwotone, ProductsMajorTwotone} from '@shopify/polaris-icons';

import {
  Autocomplete, 
  Button,
  Icon, 
  Card,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  TextField
} from '@shopify/polaris';
import {SearchMinor} from '@shopify/polaris-icons';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function AutocompleteExample() {
  //const deselectedOptions = [];
  const [deselectedOptions, setDeselectedOptions] =  useState([]);
  const [selectedOptions, setSelectedOptions] = useState(deselectedOptions);
  const [address, setAddress] = useState('');
  const [subdistrictCode, setSubdistrictCode] = useState('');
  const [city, setCity] = useState('');
  const [cityCode, setCityCode] = useState('');
  const [province, setProvince] = useState('');
  const [provinceCode, setProvinceCode] = useState('');
  const [location, setLocation] = useState([]);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [inputValue, setInputValue] = useState(deselectedOptions);
  const [options, setOptions] = useState([]);
  const handleChange = useCallback((newValue) => setValue(newValue), []);

  const updateText = useCallback(
    (value) => {
      //console.log(value)
      setInputValue(value);

      if (value === '') {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, 'i');
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex),
      );
      setOptions(resultOptions);
    },
    [deselectedOptions],
  );

  const updateSelection = useCallback((selected) => {
    const data_loc = selected.toString().split("/");
    const data_sub =  data_loc[0].split('-');
    const data_city =  data_loc[1].split('-');
    const data_prov =  data_loc[2].split('-');
    console.log(data_sub);
    // console.log(selected);
    // console.log(options);
    //console.log(selected)
    // let arr2 = deselectedOptions.filter(d => d.value === selected.toString());
    // //const data_location = JSON.parse(arr2);
    // console.log(arr2)
    // console.log(arr2[0]['province_name']);
    // // setLocation(arr2[0]);

    setSubdistrictCode(data_sub[0]);
    setInputValue(data_sub[1]);
    setCityCode(data_city[0]);
    setCity(data_city[1]);
    setProvinceCode(data_prov[0]);
    setProvince(data_prov[1]);
   
  
    // console.log(arr2[0].city_name);
    // console.log(selected.toString())
    //console.log(options)
    // const selectedValue = selected.map((selectedItem) => {
    //   const matchedOption = options.find((option) => {
    //     //console.log(option)console.log(option)
    //     return option.value.match(selectedItem);
    //   });
    //   //console.log(matchedOption.label)
    //   return matchedOption;
    // });

    //console.log(selectedValue);

    //setSelectedOptions(selected);
    //setInputValue(arr2[0].label);
  }, []);

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      label="Subdistrict"
      value={inputValue}
      prefix={<Icon source={SearchMinor} color="inkLighter" />}
      placeholder="Search"
    />
  );


  // componentDidMount() {
  //   NewsService.fetchProjects(function(articles){
  //     // load articles in the state
  //     this.setState({_articles: _articles})
  //   });
  // }

  useEffect(() => {
    //const newArr = [...deselectedOptions];
    axios.get(`http://localhost:3000/master_locations`)
      .then(res => {
        const datax = [];
        res.data.forEach(function (element) {
          element.value = element.subdistrict_code+'-'+ element.subdistrict_name+'/'+ element.city_code+'-'+ element.city_name+'/'+element.province_code+'-'+ element.province_name;
          element.label = element.subdistrict_name+' - ' + element.city_name;
        });

        setDeselectedOptions(res.data);
        //console.log(res);

        //let json = JSON.parse(JSON.stringify(res.data).split('"subdistrict_code":').join('"value":'));
        //console.log(JSON.parse(JSON.stringify(json)));
        //json = JSON.parse(JSON.stringify(json).split('"subdistrict_name":').join('"label":'));
        //console.log
        //newArr.push(json);
        //setOptions(json);
        //setDeselectOptions(state => ({ ...state, setDeselectOptions: json }));
        //setDeselectOptions(json);
        //console.log(deselectedOptions);
        //deselectedOptions = res.data;
        //console.log(json)
    })

    axios.post(`http://localhost:3000/get_location`,{
      shop: Cookies.get("shopOrigin"),
    })
    .then(res => {
        setAddress(res.data.address);
        setPhoneNumber(res.data.phone_number);
        setInputValue(res.data.subdistrict_name);
        setSubdistrictCode(res.data.subdistrict_code);
        setCityCode(res.data.cityi_code);
        setCity(res.data.city_name);
        setProvinceCode(res.data.province_code);
        setProvince(res.data.province_name);
        setZipCode(res.data.zip_code);

    })
  }, []);

  const handleAddress = useCallback((value) => setAddress(value), []);
  const handlerPhoneNumber = useCallback((value) => setPhoneNumber(value), []);
  const handlerZipcode = useCallback((value) => setZipCode(value), []);

  const handleSubmit = useCallback((_event) => {
      // const data_location = {
      //   shop : Cookies.get("shopOrigin"),
      //   phone_number: phoneNumber,
      //   address: address,
      //   zip_code: zipCode,
      //   subdistrict_code: subdistrictCode,
      //   subdistrict_name: inputValue,
      //   city_code: cityCode,
      //   city_name: city,
      //   province_code: provinceCode,
      //   province_name: province
      // }
      //console.log(data_location);
      axios.post(`http://localhost:3000/save_location`,{
        shop : Cookies.get("shopOrigin"),
        phone_number: phoneNumber,
        address: address,
        zip_code: zipCode,
        subdistrict_code: subdistrictCode,
        subdistrict_name: inputValue,
        city_code: cityCode,
        city_name: city,
        province_code: provinceCode,
        province_name: province
      })
      .then(res => {
          console.log(res)
          //console.log(res);
          // setSelected(res.data.courriers)
          // console.log(res.data.courriers)
      })
    // data_location = location;
    // console.log(data_location)
    //   console.log(address);
    //   console.log(inputValue);
    //   console.log(subdistrictCode);
    //   console.log(city);
    //   console.log(cityCode);
    //   console.log(province);
    //   console.log(provinceCode);
    //   console.log(zipCode);
    //   console.log(phoneNumber);
  }, [address, city, province, phoneNumber, zipCode]);

  return (
    <Page>
      <Layout>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <TextField
                    value={address}
                    onChange={handleAddress}
                    label="Adress (*)"
                    type="address"
                    placeholder="Address"
                  />
                <Autocomplete
                  options={options}
                  selected={selectedOptions}
                  onSelect={updateSelection}
                  textField={textField}
                />
                <TextField
                  value={city}
                  label="City (*)"
                  disabled
                />
                <TextField
                  value={province}
                  label="Province (*)"
                  disabled
                />
                <TextField
                  value={zipCode}
                  onChange={handlerZipcode}
                  label="Zip Code (*)"
                  type="number"
                  placeholder="Zip Code"
                />
                <TextField
                  value={phoneNumber}
                  onChange={handlerPhoneNumber}
                  label="Phone Number (*)"
                  placeholder="Phone Number"
                />
                <Stack distribution="trailing">
                  <Button primary submit>
                    Save
                  </Button>
                </Stack>
              </FormLayout>
            </Form>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}
