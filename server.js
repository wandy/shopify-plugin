require('isomorphic-fetch');
const Koa = require('koa');
const BodyParser = require("koa-bodyparser");
const Router = require('koa-router');
const cors = require('koa-cors');
const HttpStatus = require("http-status");
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const dotenv = require('dotenv');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const session = require('koa-session');
const mongoose = require('mongoose');
const axios = require('axios');
var querystring = require('querystring');
const serve = require('koa-static');
var Pug = require('koa-pug');
var $ = require("jquery");
const Token = require('./schema/token.js');
const Courier = require('./schema/courier.js');
const Features = require('./schema/feature.js');
const Subdistrict = require('./schema/subdistrict.js');
const StoreLocations = require('./schema/store_locations.js');

dotenv.config();

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.use(BodyParser());
  server.use(cors());
  server.use(session({ sameSite: 'none', secure: true }, server));
  server.keys = [SHOPIFY_API_SECRET_KEY];
  //server.use(serve(__dirname + 'client/build'));
  server.use(serve('./public'));

  var pug = new Pug({
    viewPath: './views',
    basedir: './views',
    app: server //Equivalent to app.use(pug)
 });

  server.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET_KEY,
      scopes: [
          'read_orders',
          'write_orders',
          'read_products',
          'write_products',
          'read_themes',
          'write_themes',
          'read_shipping',
          'write_shipping',
          'read_script_tags', 
          'write_script_tags'
        ],
       afterAuth(ctx) {
        console.log(ctx)
        const { shop, accessToken } = ctx.session;
       // token = accessToken;
        console.log(accessToken);
        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });
        ctx.cookies.set("accessToken", accessToken, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });
        ctx.redirect('/');
      },
    }),
  );
  
  //server.use(graphQLProxy({version: ApiVersion.October19}))
  //server.use(verifyRequest());
  // server.use(async (ctx) => {
  //   await handle(ctx.req, ctx.res);
  //   ctx.respond = false;
  //   ctx.res.statusCode = 200;

  // });
  // router.get('*', verifyRequest(), async (ctx) => {
 // var myEndPoint = require('./server/my-end-point.js');
  //router.use(myEndPoint .routes());

  async function generateToken(){
    const data = {
      grant_type: 'client_credentials',
      client_id: 'shipdeo-internal',
      client_secret: 'Sh1pd30',
    };
    const res = await axios.post('https://shipdeo-auth-api-development.clodeo.id/oauth2/connect/token', querystring.stringify(data));
    return res;
  }

  async function getToken(param){
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const token = await Token.findOne({shop: param}, function (err, res) {
      return res;
    });
    if(token) {
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token,
      }
      const results = await axios.post("http://shipdeo-main-api-development.clodeo.id/v1/couriers/list", {}, {
        headers: headers,
      }).then(response => { 
          return response.status;
      })
      .catch(error => {
          return error.response.status;
      });
      //console.log(results);
      if(results === 200) {
        return token.access_token;
      }else{
        res_token = await generateToken();
        console.log(res_token.data.access_token);
        const doc = await Token.findOneAndUpdate({shop:param}, {access_token: res_token.data.access_token});
        console.log(doc);
        return res_token.data.access_token;
        //return 'refresh token';
      }
    }else{
      //console.log('buat token');
      const res = await generateToken();
      const data_token = {
          shop: param, 
          access_token: res.data.access_token, 
      }
      const save_token = new Token(data_token);
      return await save_token.save();
    }
  }

  router.get("/process",async (ctx,next)=>{
      mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
      const data = await Subdistrict.find({},{},function (err, res) {
        return res;
      })
      //console.log(data);

      await ctx.render('index',{
        name: 'test', 
        subditrict: ['1','2','3']
      });
      //ctx.render('first_view');
  });

  router.post("/process",async (ctx,next)=>{
      ctx.body = 'getData';
      console.log(ctx.request.body);
  });

  router.post("/get_shipping",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const datax = ctx.request.body;
    const shop_name = new URL(datax.shop).host;
    const data_couriers = await Courier.findOne({ shop: shop_name},{},function (err, res) {
      return res;
    });
    const store_locations = await StoreLocations.findOne({ shop: shop_name},{},function (err, res) {
      return res;
    })
    //console.log(store_locations);
    const body = {
      couriers: data_couriers.couriers,
      origin_lat: '',
      origin_long: '',
      origin_subdistrict_code: store_locations.subdistrict_code,
      origin_subdistrict_name: store_locations.subdistrict_name,
      origin_city_code: store_locations.city_code,
      origin_city_name: store_locations.city_name,
      origin_postal_code: store_locations.zip_code,
      destination_lat: '',
      destination_long: '',
      destination_subdistrict_code: datax.subdistrict_code,
      destination_subdistrict_name: datax.subdistrict_name,
      destination_city_code: datax.city_code,
      destination_city_name: datax.city_name,
      destination_postal_code: datax.zip_code,
      is_cod: true,
      items: datax.items
    }
    try {
      const token = await getToken(shop_name).then(data => {
        return data;
      })
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
      const results = await axios.post("http://shipdeo-main-api-development.clodeo.id/v1/couriers/pricing", body, {
        headers: headers,
      })
      .then(res => {
       // console.log(res);
        ctx.body = res.data;
        //console.log(res.data.data);
      });
    } catch (err) {
      console.log(err)
    }
  });

  router.post('/api/list_courier', async (ctx) => {
    try {
      const token = await getToken(url).then(data => {
        return data;
      })
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
      const results = await axios.post("https://shipdeo-main-api-development/couriers/list", {}, {
        headers: headers,
      })
      .then(res => {
        ctx.body = res.data.data;
      });
    } catch (err) {
      console.log(err)
    }
  })


  router.get("/courier",async (ctx,next)=>{
    ctx.body = 'Testing';
   // ctx.redirect('http://localhost:4200/heroes');
    ctx.redirect(ctx.res, '/blog');
    // await ctx.render('/blog', {
    // });
    //return app.render(ctx.res, '/blog');
    //ctx.redirect(ctx.res, '/blog');
    //return {};
    //await next();
  });

  router.post("/manage_courier",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("Connection Successful!");
        const datax = ctx.request.body;
        const data_courier = {
            shop: datax.shop, 
            couriers: datax.courier 
        }
        var save_courier = new Courier(data_courier);
        var query = { shop: datax.shop };
        const data = Courier.findOneAndUpdate(query ,data_courier,(error, doc) => {
          if(doc == null) {
            save_courier.save(function (err, courier) {
              if (err) return console.error(err);
              console.log(courier.shop + " saved to courier collection.");
              ctx.body = 'saved to courier collection.';
            });
          }
        });
    });
    ctx.body = 'test';
  });


  router.post("/manage_feature",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("Connection Successful!");
        const datax = ctx.request.body;
        const data_features = {
            shop: datax.shop, 
            features: datax.courier
        }
        var save_feature = new Features(data_features);
        var query = { shop: datax.shop };
        console.log(query);
        const data = Features.findOneAndUpdate(query ,data_features,(error, doc) => {
          console.log(doc);
          if(doc == null) {
            save_feature.save(function (err, feature) {
              if (err) return console.error(err);
              console.log(feature.shop + " saved to feature collection.");
              ctx.body = 'saved to feature collection.';
            });
          }
        });
        // save model to database
    });
    ctx.body = 'test';
  });

  router.post("/get_features",async (ctx,next)=>{
    const data_courier = ctx.request.body;
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Features.findOne( { shop: data_courier.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;
  
  });

  
  router.post("/save_location",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        const datax = ctx.request.body;
        data_location = { 
          shop: datax.shop, 
          phone_number: datax.phone_number, 
          address: datax.address, 
          zip_code: datax.zip_code, 
          subdistrict_code: datax.subdistrict_code, 
          subdistrict_name: datax.subdistrict_name, 
          city_code: datax.city_code, 
          city_name: datax.city_name, 
          province_code: datax.province_code, 
          province_name: datax.province_name, 
        }
        var save_location = new StoreLocations(data_location);
        var query = { shop: datax.shop };
        const data = StoreLocations.findOneAndUpdate(query ,data_location,(error, doc) => {
          if(doc == null) {
            save_location.save(function (err, location) {
              if (err) return console.error(err);
              console.log(location.shop + " saved to bookstore collection.");
              ctx.body = 'saved to bookstore collection.';
            });
          }
        });
    });
    ctx.body = 'test';
  });

  router.post("/get_location",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data_store = ctx.request.body;
    console.log(data_store);
    const data = await StoreLocations.findOne( { shop: data_store.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;

  });
  


  router.get("/master_locations", async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Subdistrict.find({},{},function (err, res) {
      return res;
    })
    ctx.body = data;

  });

  router.post("/get_courier",async (ctx,next)=>{
    const data_courier = ctx.request.body;
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Courier.findOne( { shop: data_courier.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;
  
  });
  
  //router.get('/(.*)', async (ctx, next) => {
  router.get('/(.*)', async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  //router.post('/(.*)', verifyRequest(), async (ctx) => {
  router.post('/(.*)', async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  server.use(router.allowedMethods());
  server.use(router.routes());

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});